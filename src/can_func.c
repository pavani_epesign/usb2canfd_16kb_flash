#include "can_registers.h"
#include "spi_0.h"
#include "efm8_config.h"

#include "can_api.h"
#include "can_defines.h"
//uint32_t data u32_test;
//SI_SEGMENT_VARIABLE(crc16_table,uint16_t,SI_SEG_XDATA);
// *****************************************************************************
// *****************************************************************************
// Section: Defines
// Control Register Reset Values up to FIFOs
//const uint32_t canControlResetValues[20] = {
//   /* Address 0x000 to 0x00C */
//    0x04980760, 0x003E0F0F, 0x000E0303, 0x00021000,
//    /* Address 0x010 to 0x01C */
//    0x00000000, 0x00000000, 0x40400040, 0x00000000,
//   /* Address 0x020 to 0x02C */
//    0x00000000, 0x00000000, 0x00000000, 0x00000000,
//   /* Address 0x030 to 0x03C */
//   0x00000000, 0x00200000, 0x00000000, 0x00000000,
//   /* Address 0x040 to 0x04C */
//   0x00000400, 0x00000000, 0x00000000, 0x00000000
//};

// FIFO Register Reset Values
//const uint32_t canFifoResetValues[3] =
//  { 0x00600400, 0x00000000, 0x00000000 };

// Filter and Mask Object Reset Values
//const uint32_t canFilterObjectResetValues[2] =
//  { 0x00000000, 0x00000000 };

SI_SBIT(CS_PIN, SFR_P0, 3);
// *****************************************************************************
// *****************************************************************************
// Section: Variables

SI_SEGMENT_VARIABLE(spiTransmitBuffer[SPI_DEFAULT_BUFFER_LENGTH], uint8_t,
                    EFM8PDL_SPI0_TX_SEGTYPE);
SI_SEGMENT_VARIABLE(spiReceiveBuffer[SPI_DEFAULT_BUFFER_LENGTH], uint8_t,
                    EFM8PDL_SPI0_RX_SEGTYPE);

int8_t
DRV_SPI_TransferData (uint8_t spiSlaveDeviceIndex, uint8_t *SpiTxData,
                      uint8_t *SpiRxData, uint16_t spiTransferSize)
{
  uint8_t retval = 0;
  spiSlaveDeviceIndex = 0;

   CS_PIN = 0;
  //while(!SPI0CN0_NSSMD0);

   //SPI0_transfer(SpiTxData, SpiRxData, SPI0_TRANSFER_TX,spiTransferSize);
  retval = SPI0_pollTransfer (SpiTxData, SpiRxData, SPI0_TRANSFER_RXTX,
                        spiTransferSize);

  //while(!SPI0CN0_NSSMD0);

  CS_PIN = 1;

  return retval;
}

// *****************************************************************************
// *****************************************************************************
// Section: Reset

int8_t
DRV_CANFDSPI_Reset (CANFDSPI_MODULE_ID index)
{
  uint16_t spiTransferSize = 2;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) (cINSTRUCTION_RESET << 4);
  spiTransmitBuffer[1] = 0;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

// *****************************************************************************
// *****************************************************************************
// Section: SPI Access Functions

int8_t
DRV_CANFDSPI_ReadByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t *rxd)
{
  uint16_t spiTransferSize = 3;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  spiTransmitBuffer[2] = 0;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  // Update data
  *rxd = spiReceiveBuffer[2];

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t txd)
{
  uint16_t spiTransferSize = 3;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);
  spiTransmitBuffer[2] = txd;

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadWord (CANFDSPI_MODULE_ID index, uint16_t address,
                       uint32_t *rxd)
{
  uint8_t i;
  uint32_t x;
  uint16_t spiTransferSize = 6;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Update data
  *rxd = 0;
  for (i = 2; i < 6; i++)
    {
      x = (uint32_t) spiReceiveBuffer[i];
      *rxd += x << ((i - 2) * 8);
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteWord (CANFDSPI_MODULE_ID index, uint16_t address,
                        uint32_t txd)
{
  uint8_t i;
  uint16_t spiTransferSize = 6;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Split word into 4 bytes and add them to buffer
  for (i = 0; i < 4; i++)
    {
      spiTransmitBuffer[i + 2] = (uint8_t) ((txd >> (i * 8)) & 0xFF);
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                           uint16_t *rxd)
{
  uint8_t i;
  uint32_t x;
  uint16_t spiTransferSize = 4;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Update data
  *rxd = 0;
  for (i = 2; i < 4; i++)
    {
      x = (uint32_t) spiReceiveBuffer[i];
      *rxd += x << ((i - 2) * 8);
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint16_t txd)
{
  uint8_t i;
  uint16_t spiTransferSize = 4;
  int8_t spiTransferError = 0;

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Split word into 2 bytes and add them to buffer
  for (i = 0; i < 2; i++)
    {
      spiTransmitBuffer[i + 2] = (uint8_t) ((txd >> (i * 8)) & 0xFF);
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint8_t *rxd, uint16_t nBytes)
{
  uint16_t i;
  uint16_t spiTransferSize = nBytes + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_READ << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Clear data
  for (i = 2; i < spiTransferSize; i++)
    {
      spiTransmitBuffer[i] = 0;
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  // Update data
  for (i = 0; i < nBytes; i++)
    {
      rxd[i] = spiReceiveBuffer[i + 2];
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint8_t *txd, uint16_t nBytes)
{
  uint16_t i;
  uint16_t spiTransferSize = nBytes + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (uint8_t) ((cINSTRUCTION_WRITE << 4)
      + ((address >> 8) & 0xF));
  spiTransmitBuffer[1] = (uint8_t) (address & 0xFF);

  // Add data
  for (i = 0; i < nBytes; i++)
    {
      spiTransmitBuffer[i + 2] = txd[i];
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReadWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint32_t *rxd, uint16_t nWords)
{
  uint16_t i, j, n;
  REG_t w;
  uint16_t spiTransferSize = nWords * 4 + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (cINSTRUCTION_READ << 4) + ((address >> 8) & 0xF);
  spiTransmitBuffer[1] = address & 0xFF;

  // Clear data
  for (i = 2; i < spiTransferSize; i++)
    {
      spiTransmitBuffer[i] = 0;
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);
  if (spiTransferError)
    {
      return spiTransferError;
    }

  // Convert Byte array to Word array
  n = 2;
  for (i = 0; i < nWords; i++)
    {
      w.word = 0;
      for (j = 0; j < 4; j++, n++)
        {
          w.byte[j] = spiReceiveBuffer[n];
        }
      rxd[i] = w.word;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_WriteWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint32_t *txd, uint16_t nWords)
{
  uint16_t i, j, n;
  REG_t w;
  uint16_t spiTransferSize = nWords * 4 + 2;
  int8_t spiTransferError = 0;

  // Validate that length of array is sufficient to hold requested number of bytes
  if (spiTransferSize > sizeof(spiTransmitBuffer))
    {
      return -1;
    }

  // Compose command
  spiTransmitBuffer[0] = (cINSTRUCTION_WRITE << 4) + ((address >> 8) & 0xF);
  spiTransmitBuffer[1] = address & 0xFF;

  // Convert ByteArray to word array
  n = 2;
  for (i = 0; i < nWords; i++)
    {
      w.word = txd[i];
      for (j = 0; j < 4; j++, n++)
        {
          spiTransmitBuffer[n] = w.byte[j];
        }
    }

  spiTransferError = DRV_SPI_TransferData (index, spiTransmitBuffer,
                                           spiReceiveBuffer, spiTransferSize);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_EccEnable (CANFDSPI_MODULE_ID index)
{
  int8_t spiTransferError = 0;
  uint8_t d = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_ECCCON, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d |= 0x01;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_ECCCON, d);
  if (spiTransferError)
    {
      return -2;
    }

  return 0;
}

int8_t
DRV_CANFDSPI_RamInit (CANFDSPI_MODULE_ID index, uint8_t d)
{
  uint8_t txd[SPI_DEFAULT_BUFFER_LENGTH / 2];
  uint32_t k;
  uint16_t a;
  int8_t spiTransferError = 0;

  // Prepare data
  for (k = 0; k < SPI_DEFAULT_BUFFER_LENGTH / 2; k++)
    {
      txd[k] = d;
    }

  a = cRAMADDR_START;

  for (k = 0; k < ((cRAM_SIZE / SPI_DEFAULT_BUFFER_LENGTH) * 2); k++)
    {
      spiTransferError = DRV_CANFDSPI_WriteByteArray (
          index, a, txd, SPI_DEFAULT_BUFFER_LENGTH / 2);
      if (spiTransferError)
        {
          return -1;
        }
      a += SPI_DEFAULT_BUFFER_LENGTH / 2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_Configure (CANFDSPI_MODULE_ID index)
{
  REG_CiCON cicon;
  int8_t spiTransferError = 0;
  uint32_t wd;
  uint32_t wd1, wd2;

  //wd = 0x04980760;                      // Reset Value
  cicon.word = 0x0760;                            // Reset Value
//  cicon.bF1.DNetFilterCount = config->bF1.DNetFilterCount;
//  cicon.bF1.IsoCrcEnable = config->bF1.IsoCrcEnable;
//  ;
//  cicon.bF1.ProtocolExceptionEventDisable =
//      config->bF1.ProtocolExceptionEventDisable;
//  cicon.bF1.WakeUpFilterEnable = config->bF1.WakeUpFilterEnable;
//  cicon.bF1.WakeUpFilterTime = config->bF1.WakeUpFilterTime;
//  cicon.bF1.BitRateSwitchDisable = config->bF1.BitRateSwitchDisable;
  wd1 = cicon.word;
  cicon.word = 0x0490;                          // Reset Value
//  cicon.bF2.RestrictReTxAttempts = config->bF2.RestrictReTxAttempts;
//  cicon.bF2.EsiInGatewayMode = config->bF2.EsiInGatewayMode;
//  cicon.bF2.SystemErrorToListenOnly = config->bF2.SystemErrorToListenOnly;
//  cicon.bF2.StoreInTEF = config->bF2.StoreInTEF;
//  cicon.bF2.TXQEnable = config->bF2.TXQEnable;
//  cicon.bF2.AbortAllTx = config->bF2.AbortAllTx;
//  cicon.bF2.TxBandWidthSharing = config->bF2.TxBandWidthSharing;
  wd2 = cicon.word;

  wd = wd1 + (wd2 << 16);
  //ciCon.word = 0x04900760;
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiCON, wd);
//  DRV_CANFDSPI_ReadWord(index,cREGADDR_CiCON,&u32_test);
  if (spiTransferError)
    {
      return -1;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelConfigure (CANFDSPI_MODULE_ID index, uint8_t channel,
                                      CAN_RX_FIFO_CONFIG *config)
{
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint32_t wd;
  uint32_t wd1, wd2;

  if (channel == CAN_TXQUEUE_CH0)
    {
      return -100;
    }

  // Setup FIFO
  ciFifoCon.word = 0x0400;

  wd1 = ciFifoCon.word;

  ciFifoCon.word = 0x0f60;
  ciFifoCon.rxBF2.PayLoadSize = config->PayLoadSize;
  wd2 = ciFifoCon.word;

  wd = wd1 + (wd2 << 16);
  //ciFifoCon.word = 0xef600400;

  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_NominalBitTimeConfigure (CANFDSPI_MODULE_ID index,
                                      CAN_BITTIME_SETUP bitTime,
                                      CAN_SYSCLK_SPEED clk)
{
  int8_t spiTransferError = 0;

  // Decode clk

  if (clk == CAN_SYSCLK_20M)
    {
      spiTransferError = DRV_CANFDSPI_BitTimeConfigureNominal20MHz (index,
                                                                    bitTime);

    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_DataBitTimeConfigure (CANFDSPI_MODULE_ID index,
                                   CAN_BITTIME_SETUP bitTime,
                                   CAN_SYSCLK_SPEED clk)
{
  int8_t spiTransferError = 0;

  // Decode clk

  if (clk == CAN_SYSCLK_20M)
    {

      spiTransferError = DRV_CANFDSPI_BitTimeConfigureData20MHz (index,
                                                                 bitTime);

    }
  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureNominal20MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t wd, wd1, wd2;
  REG_CiNBTCFG ciNbtcfg;

  //ciNbtcfg.word = canControlResetValues[cREGADDR_CiNBTCFG / 4];

  // Arbitration Bit rate
  switch (bitTime)
    {
// All 500K
    case CAN_500K_1M:
    case CAN_500K_2M:
    case CAN_500K_4M:
    case CAN_500K_5M:
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
      ciNbtcfg.word = 0x001E;
//      ciNbtcfg.bF2.BRP = 0;
//      ciNbtcfg.bF2.TSEG1 = 30;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0707;
//      ciNbtcfg.bF1.TSEG2 = 7;
//      ciNbtcfg.bF1.SJW = 7;
      wd1 = ciNbtcfg.word;
      break;
      // All 250K
    case CAN_250K_500K:
    case CAN_250K_833K:
    case CAN_250K_1M:
    case CAN_250K_1M5:
    case CAN_250K_2M:
    case CAN_250K_3M:
    case CAN_250K_4M:
      ciNbtcfg.word = 0x003E;
//      ciNbtcfg.bF2.BRP = 0;
//      ciNbtcfg.bF2.TSEG1 = 62;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0F0F;
//      ciNbtcfg.bF1.TSEG2 = 15;
//      ciNbtcfg.bF1.SJW = 15;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_1000K_4M:
    case CAN_1000K_8M:

      ciNbtcfg.word = 0x000E;
//      ciNbtcfg.bF2.BRP = 0;
//      ciNbtcfg.bF2.TSEG1 = 14;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x0303;
//      ciNbtcfg.bF1.TSEG2 = 3;
//      ciNbtcfg.bF1.SJW = 3;
      wd1 = ciNbtcfg.word;
      break;

    case CAN_125K_500K:
      ciNbtcfg.word = 0x007E;
//      ciNbtcfg.bF2.BRP = 0;
//      ciNbtcfg.bF2.TSEG1 = 126;
      wd2 = ciNbtcfg.word;
      ciNbtcfg.word = 0x1F1F;
//      ciNbtcfg.bF1.TSEG2 = 31;
//      ciNbtcfg.bF1.SJW = 31;
      wd1 = ciNbtcfg.word;
      break;

    default:
      return -1;
      break;
    }
  wd = wd1 + (wd2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiNBTCFG, wd);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_BitTimeConfigureData20MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime)
{
  int8_t spiTransferError = 0;
  uint32_t tdcValue = 0;
  uint32_t wdDbtc, wdDbtc1, wdDbtc2;
  REG_CiDBTCFG ciDbtcfg;
  uint32_t wdTDC, wdTDC1, wdTDC2;
  REG_CiTDC ciTdc;
  //    sspMode;

  //ciDbtcfg.word = canControlResetValues[cREGADDR_CiDBTCFG / 4];
  ciTdc.word = 0;

  // Configure Bit time and sample point
  ciTdc.bF2.TDCMode = CAN_SSP_MODE_AUTO;
  wdTDC2 = ciTdc.word;

  // Data Bit rate and SSP
  switch (bitTime)
    {
    case CAN_500K_1M:
      ciDbtcfg.word = 0x000E;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
//      ciDbtcfg.bF1.TSEG2 = 3;
//      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_2M:
      // Data BR
      ciDbtcfg.word = 0x0006;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0101;
//      ciDbtcfg.bF1.TSEG2 = 1;
//      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_4M:
    case CAN_1000K_4M:
      // Data BR
      ciDbtcfg.word = 0x0002;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0000;
//      ciDbtcfg.bF1.TSEG2 = 0;
//      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_5M:
      // Data BR
      ciDbtcfg.word = 0x0001;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 1;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0000;
//      ciDbtcfg.bF1.TSEG2 = 0;
//      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 2;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_500K_6M7:
    case CAN_500K_8M:
    case CAN_500K_10M:
    case CAN_1000K_8M:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;

    case CAN_250K_500K:
    case CAN_125K_500K:
      ciDbtcfg.word = 0x001E;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 30;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0707;
//      ciDbtcfg.bF1.TSEG2 = 7;
//      ciDbtcfg.bF1.SJW = 7;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 31;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_833K:
      ciDbtcfg.word = 0x0011;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 17;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0404;
//      ciDbtcfg.bF1.TSEG2 = 4;
//      ciDbtcfg.bF1.SJW = 4;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 18;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      ciTdc.word = wdDbtc2;
      ciTdc.bF2.TDCMode = CAN_SSP_MODE_OFF;
      wdTDC2 = ciTdc.word;
      break;
    case CAN_250K_1M:
      ciDbtcfg.word = 0x000E;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 14;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0303;
//      ciDbtcfg.bF1.TSEG2 = 3;
//      ciDbtcfg.bF1.SJW = 3;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 15;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_1M5:
      ciDbtcfg.word = 0x0008;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 8;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0202;
//      ciDbtcfg.bF1.TSEG2 = 2;
//      ciDbtcfg.bF1.SJW = 2;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 9;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_2M:
      ciDbtcfg.word = 0x0006;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 6;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0101;
//      ciDbtcfg.bF1.TSEG2 = 1;
//      ciDbtcfg.bF1.SJW = 1;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 7;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;
    case CAN_250K_3M:
      //qDebug("Data Bitrate not feasible with this clock!");
      return -1;
      break;
    case CAN_250K_4M:
      // Data BR
      ciDbtcfg.word = 0x0002;
//      ciDbtcfg.bF2.BRP = 0;
//      ciDbtcfg.bF2.TSEG1 = 2;
      wdDbtc2 = ciDbtcfg.word;
      ciDbtcfg.word = 0x0000;
//      ciDbtcfg.bF1.TSEG2 = 0;
//      ciDbtcfg.bF1.SJW = 0;
      wdDbtc1 = ciDbtcfg.word;
      // SSP
      ciTdc.word = 0;
      ciTdc.bF1.TDCOffset = 3;
      ciTdc.bF1.TDCValue = tdcValue;
      wdTDC1 = ciTdc.word;
      break;

    default:
      return -1;
      break;
    }

  wdDbtc = wdDbtc1 + (wdDbtc2 << 16);
  // Write Bit time registers
  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiDBTCFG, wdDbtc);
  if (spiTransferError)
    {
      return -2;
    }

  // Write Transmitter Delay Compensation
#ifdef REV_A
    ciTdc.bF.TDCOffset = 0;
    ciTdc.bF.TDCValue = 0;
#endif

  wdTDC = wdTDC1 + (wdTDC2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, cREGADDR_CiTDC, wdTDC);
  if (spiTransferError)
    {
      return -3;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_OperationModeSelect (CANFDSPI_MODULE_ID index,
                                  CAN_OPERATION_MODE opMode)
{
  uint8_t d = 0;
  int8_t spiTransferError = 0;

  // Read
  spiTransferError = DRV_CANFDSPI_ReadByte (index, cREGADDR_CiCON + 3, &d);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  d &= ~0x07;
  d |= opMode;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, cREGADDR_CiCON + 3, d);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterObjectConfigure (CANFDSPI_MODULE_ID index, uint8_t filter,
                                    CAN_FILTEROBJ_ID *id)
{
  uint16_t a;
  uint32_t wd, wd1, wd2;
  REG_CiFLTOBJ fObj;
  int8_t spiTransferError = 0;

  // Setup
  fObj.word = 0;
  fObj.bF.bF1 = id->bF1;
  wd1 = (fObj.word) >> 16;
  fObj.word = 0;
  fObj.bF.bF2 = id->bF2;
  wd2 = fObj.word;
  //fObj.word = 0xda;
  a = cREGADDR_CiFLTOBJ + (filter * CiFILTER_OFFSET);

  wd = wd1 + (wd2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterMaskConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                  CAN_MASKOBJ_ID *mask)
{
  uint16_t a;
  uint32_t wd, wd1, wd2;
  REG_CiMASK mObj;
  int8_t spiTransferError = 0;

  // Setup
  mObj.word = 0;
  mObj.bF.bF1 = mask->bF1;
  wd1 = (mObj.word) >> 16;
  mObj.word = 0;
  mObj.bF.bF2 = mask->bF2;
  wd2 = mObj.word;
  //mObj.word = 0x40000000;
  a = cREGADDR_CiMASK + (filter * CiFILTER_OFFSET);

  wd = wd1 + (wd2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterToFifoLink (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                               CAN_FIFO_CHANNEL channel, bool enable)
{
  uint16_t a;
  REG_CiFLTCON_BYTE fCtrl;
  int8_t spiTransferError = 0;

  // Enable
  if (enable)
    {
      fCtrl.bF.Enable = 1;
    }
  else
    {
      fCtrl.bF.Enable = 0;
    }

  // Link
  fCtrl.bF.BufferPointer = channel;
  //fCtrl.byte = 129;                    //0x81
  a = cREGADDR_CiFLTCON + filter;

  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, fCtrl.byte);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioModeConfigure (CANFDSPI_MODULE_ID index, GPIO_PIN_MODE gpio0,
                                GPIO_PIN_MODE gpio1)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t byte;
  REG_IOCON iocon;

  // Read
  a = cREGADDR_IOCON + 3;
  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF2.PinMode0 = gpio0;
  iocon.bF2.PinMode1 = gpio1;
  byte = iocon.byte[0];
  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, byte);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_GpioDirectionConfigure (CANFDSPI_MODULE_ID index,
                                     GPIO_PIN_DIRECTION gpio0,
                                     GPIO_PIN_DIRECTION gpio1)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint8_t byte;
  REG_IOCON iocon;

  // Read
  a = cREGADDR_IOCON;
  iocon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &iocon.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  iocon.bF1.TRIS0 = gpio0;
  iocon.bF1.TRIS1 = gpio1;
  byte = iocon.byte[1];
  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, byte);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelEventEnable (CANFDSPI_MODULE_ID index,
                                        CAN_FIFO_CHANNEL channel,
                                        CAN_RX_FIFO_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiFIFOCON ciFifoCon;
  if (channel == CAN_TXQUEUE_CH0)
    return -100;

  // Read Interrupt Enables
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  ciFifoCon.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciFifoCon.byte[1]);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  ciFifoCon.byte[1] |= (flags & CAN_RX_FIFO_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciFifoCon.byte[1]);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelStatusGet (CANFDSPI_MODULE_ID index,
                                      CAN_FIFO_CHANNEL channel,
                                      CAN_RX_FIFO_STATUS *status)
{
  uint16_t a;
  REG_CiFIFOSTA1 ciFifoSta;
  int8_t spiTransferError = 0;

  // Read
  ciFifoSta.word = 0;
  a = cREGADDR_CiFIFOSTA + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciFifoSta.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *status = (CAN_RX_FIFO_STATUS) (ciFifoSta.byte[0] & 0x0F);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ModuleEventEnable (CANFDSPI_MODULE_ID index,
                                CAN_MODULE_EVENT flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiINTENABLE intEnables;
  // Read Interrupt Enables
  a = cREGADDR_CiINTENABLE;

  intEnables.word = 0;

  spiTransferError = DRV_CANFDSPI_ReadHalfWord (index, a, &intEnables.word);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  intEnables.word |= (flags & CAN_ALL_EVENTS);

  // Write
  spiTransferError = DRV_CANFDSPI_WriteHalfWord (index, a, intEnables.word);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelEventGet (CANFDSPI_MODULE_ID index, uint8_t channel,
                                     CAN_RX_FIFO_EVENT *flags)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  REG_CiFIFOSTA1 ciFifoSta;
  if (channel == CAN_TXQUEUE_CH0)
    return -100;

  // Read Interrupt flags

  ciFifoSta.word = 0;
  a = cREGADDR_CiFIFOSTA + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &ciFifoSta.byte[0]);
  if (spiTransferError)
    {
      return -1;
    }

  // Update data
  *flags = (CAN_RX_FIFO_EVENT) (ciFifoSta.byte[0] & CAN_RX_FIFO_ALL_EVENTS);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveMessageGet (CANFDSPI_MODULE_ID index, uint8_t channel,
                                CAN_RX_MSGOBJ *rxObj, uint8_t *rxd,
                                uint8_t nBytes)
{
  uint8_t *u8p;
  uint8_t n = 0;
  uint8_t i = 0;
  uint8_t ba[MAX_MSG_SIZE];
  REG_t myReg;
  uint16_t a;
  uint32_t fifoReg[3];
  REG_CiFIFOCON1 ciFifoCon;
  REG_CiFIFOSTA1 ciFifoSta;
  REG_CiFIFOUA1 ciFifoUa;
  int8_t spiTransferError = 0;

  // Get FIFO registers
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  spiTransferError = DRV_CANFDSPI_ReadWordArray (index, a, fifoReg, 3);
  if (spiTransferError)
    {
      return -1;
    }

  // Check that it is a receive buffer
  ciFifoCon.word = fifoReg[0];
  u8p = (uint8_t*) &ciFifoCon.word;
  if (ciFifoCon.byte[0] & 0x80)
    {
      return -2;
    }

// Get Status
  ciFifoSta.word = fifoReg[1];

  // Get address
  ciFifoUa.word = fifoReg[2];
#ifdef USERADDRESS_TIMES_FOUR
    a = 4 * ciFifoUa.bF.UserAddress;
#else
  a = ciFifoUa.byte[0] + ((((uint16_t) ciFifoUa.byte[1]) << 8) & 0xF00);
#endif
  a += cRAMADDR_START;

  // Number of bytes to read
  n = nBytes + 8; // Add 8 header bytes

  if ((ciFifoCon.byte[0] >> 5) & 0x1)
    {
      n += 4; // Add 4 time stamp bytes
    }

  // Make sure we read a multiple of 4 bytes from RAM
  if (n % 4)
    {
      n = n + 4 - (n % 4);
    }

  // Read rxObj using one access

  if (n > MAX_MSG_SIZE)
    {
      n = MAX_MSG_SIZE;
    }

  spiTransferError = DRV_CANFDSPI_ReadByteArray (index, a, ba, n);
  if (spiTransferError)
    {
      return -3;
    }

  // Assign message header

  myReg.byte[0] = ba[0];
  myReg.byte[1] = ba[1];
  myReg.byte[2] = ba[2];
  myReg.byte[3] = ba[3];
  rxObj->word[0] = myReg.word;

  myReg.byte[0] = ba[4];
  myReg.byte[1] = ba[5];
  myReg.byte[2] = ba[6];
  myReg.byte[3] = ba[7];
  rxObj->word[1] = myReg.word;

//  if ((ciFifoCon.word >> 5) & 0x1)
  if ((ciFifoCon.byte[0] >> 5) & 0x1)
    {
      myReg.byte[0] = ba[8];
      myReg.byte[1] = ba[9];
      myReg.byte[2] = ba[10];
      myReg.byte[3] = ba[11];
      rxObj->word[2] = myReg.word;

      // Assign message data
      for (i = 0; i < nBytes; i++)
        {
          rxd[i] = ba[i + 12];
        }
    }
  else
    {
      rxObj->word[2] = 0;

      // Assign message data
      for (i = 0; i < nBytes; i++)
        {
          rxd[i] = ba[i + 8];
        }
    }

  // UINC channel
  spiTransferError = DRV_CANFDSPI_ReceiveChannelUpdate (index, channel);
  if (spiTransferError)
    {
      return -4;
    }

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_ReceiveChannelUpdate (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel)
{
  uint16_t a = 0;
  REG_CiFIFOCON ciFifoCon;
  int8_t spiTransferError = 0;
  ciFifoCon.word = 0;

  // Set UINC
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET) + 1; // Byte that contains FRESET
//    ciFifoCon.rxBF.UINC = 1;
  ciFifoCon.word = 0x100;
  // Write byte
//  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, ciFifoCon.byte[1]);
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, 1);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_TransmitChannelConfigure (CANFDSPI_MODULE_ID index,
                                       uint8_t channel,
                                       CAN_TX_FIFO_CONFIG *config)
{
  int8_t spiTransferError = 0;
  uint16_t a = 0;
  uint32_t wd, wd1, wd2;

  // Setup FIFO
  REG_CiFIFOCON ciFifoCon;
  ciFifoCon.word = 0x0480;
  wd1 = ciFifoCon.word;
  ciFifoCon.word = 0x0760;
  ciFifoCon.txBF2.PayLoadSize = config->PayLoadSize;
  wd2 = ciFifoCon.word;
  //ciFifoCon.word = 0xe7610480;
  a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

  wd = wd1 + (wd2 << 16);

  spiTransferError = DRV_CANFDSPI_WriteWord (index, a, wd);

  return spiTransferError;
}

int8_t
DRV_CANFDSPI_FilterDisable (CANFDSPI_MODULE_ID index, CAN_FILTER filter)
{
  uint16_t a;
  REG_CiFLTCON_BYTE fCtrl;
  int8_t spiTransferError = 0;

  // Read
  a = cREGADDR_CiFLTCON + filter;

  spiTransferError = DRV_CANFDSPI_ReadByte (index, a, &fCtrl.byte);
  if (spiTransferError)
    {
      return -1;
    }

  // Modify
  fCtrl.bF.Enable = 0;

  // Write
  spiTransferError = DRV_CANFDSPI_WriteByte (index, a, fCtrl.byte);
  if (spiTransferError)
    {
      return -2;
    }

  return spiTransferError;
}

int8_t DRV_CANFDSPI_TransmitChannelEventGet(CANFDSPI_MODULE_ID index,
        CAN_FIFO_CHANNEL channel, CAN_TX_FIFO_EVENT* flags)
{
    int8_t spiTransferError = 0;
    uint16_t a = 0;

    // Read Interrupt flags
    REG_CiFIFOSTA1 ciFifoSta;
    ciFifoSta.word = 0;
    a = cREGADDR_CiFIFOSTA + (channel * CiFIFO_OFFSET);

    spiTransferError = DRV_CANFDSPI_ReadByte(index, a, &ciFifoSta.byte[0]);
    if (spiTransferError) {
        return -1;
    }

    // Update data
    *flags = (CAN_TX_FIFO_EVENT) (ciFifoSta.byte[0] & CAN_TX_FIFO_ALL_EVENTS);

    return spiTransferError;
}

int8_t DRV_CANFDSPI_TransmitChannelUpdate(CANFDSPI_MODULE_ID index,
        CAN_FIFO_CHANNEL channel, bool flush)
{
    uint16_t a;
    REG_CiFIFOCON1 ciFifoCon;
    int8_t spiTransferError = 0;

    // Set UINC
    a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET) + 1; // Byte that contains FRESET
    ciFifoCon.word = 0;
   // ciFifoCon.txBF1.UINC = 1;
    //ciFifoCon.byte[1] = 0x01;

    // Set TXREQ
    if (flush) {
        ciFifoCon.byte[1] = 0x03;
    }

    spiTransferError = DRV_CANFDSPI_WriteByte(index, a, ciFifoCon.byte[1]);
    if (spiTransferError) {
        return -1;
    }

    return spiTransferError;
}


int8_t DRV_CANFDSPI_TransmitChannelLoad(CANFDSPI_MODULE_ID index,
        CAN_FIFO_CHANNEL channel, CAN_TX_MSGOBJ* txObj,
        uint8_t *txd, uint32_t txdNumBytes, bool flush)
{
    //uint8_t *u8p;
    uint16_t a;
    uint32_t fifoReg[3];
//    uint32_t dataBytesInObject;
    REG_CiFIFOCON1 ciFifoCon;
     REG_CiFIFOSTA1 ciFifoSta;
    REG_CiFIFOUA1 ciFifoUa;
    int8_t spiTransferError = 0;
    uint8_t txBuffer[MAX_MSG_SIZE];
    uint8_t i;
    uint16_t n = 0;
    uint8_t j = 0;

    // Get FIFO registers
    a = cREGADDR_CiFIFOCON + (channel * CiFIFO_OFFSET);

    spiTransferError = DRV_CANFDSPI_ReadWordArray(index, a, fifoReg, 3);
    if (spiTransferError) {
        return -1;
    }

    // Check that it is a transmit buffer
    ciFifoCon.word = fifoReg[0];
    //u8p = (uint8_t*) &ciFifoCon.word;
    if (!(ciFifoCon.byte[0] & 0x80)) {
        return -2;
    }

    // Check that DLC is big enough for data
//    dataBytesInObject = DRV_CANFDSPI_DlcToDataBytes((CAN_DLC) txObj->bF.ctrl.DLC);
//    if (dataBytesInObject < txdNumBytes) {
//        return -3;
//    }

    // Get status
    ciFifoSta.word = fifoReg[1];

    // Get address
    ciFifoUa.word = fifoReg[2];
#ifdef USERADDRESS_TIMES_FOUR
    a = 4 * ciFifoUa.bF.UserAddress;
#else
    a = ciFifoUa.byte[0] + ((((uint16_t) ciFifoUa.byte[1]) << 8) & 0xF00);
#endif
    a += cRAMADDR_START;



    txBuffer[0] = txObj->byte[0]; //not using 'for' to reduce no of instructions
    txBuffer[1] = txObj->byte[1];
    txBuffer[2] = txObj->byte[2];
    txBuffer[3] = txObj->byte[3];

    txBuffer[4] = txObj->byte[4];
    txBuffer[5] = txObj->byte[5];
    txBuffer[6] = txObj->byte[6];
    txBuffer[7] = txObj->byte[7];


    for (i = 0; i < txdNumBytes; i++) {
        txBuffer[i + 8] = txd[i];
    }

    // Make sure we write a multiple of 4 bytes to RAM


    if (txdNumBytes % 4) {
        // Need to add bytes
        n = 4 - (txdNumBytes % 4);
        i = txdNumBytes + 8;

        for (j = 0; j < n; j++) {
            txBuffer[i + 8 + j] = 0;
        }
    }

    spiTransferError = DRV_CANFDSPI_WriteByteArray(index, a, txBuffer, txdNumBytes + 8 + n);
    if (spiTransferError) {
        return -4;
    }

    // Set UINC and TXREQ
    spiTransferError = DRV_CANFDSPI_TransmitChannelUpdate(index, channel, flush);
    if (spiTransferError) {
        return -5;
    }

    return spiTransferError;
}
