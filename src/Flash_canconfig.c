/*
 * Flash_canconfig.c
 *
 *  Created on: Jan 26, 2024
 *      Author: vgnsi
 */
#include "can_api.h"
#include "can_defines.h"
#include "EFM8BB2_FlashPrimitives.h"
#include "EFM8BB2_FlashUtils.h"
#include "uart.h"
//#include "Flash_canconfig.h";

void
CANFD_Flashdata ()
{
  uint8_t flag = 0;
  flag = FLASH_ByteRead (START_ADDR);
  if (flag == 0xff)
    {
      FLASH_PageErase (START_ADDR);
      FLASH_ByteWrite (CAN_TXCH_ADDR, 2);
      FLASH_ByteWrite (CAN_PAYLOAD_ADDR, 16);
      FLASH_ByteWrite (CAN_FILTER_ADDR, 0);
      FLASH_ByteWrite (CAN_FILTER_ID_ADDR, 0xDA);
      FLASH_ByteWrite (CAN_MASK_ID_ADDR, 0x00);
      FLASH_ByteWrite (CAN_RXCH_ADDR, 1);
      Flash_2BWrite (CAN_NBITTIME_ADDR, 250);
      Flash_2BWrite (CAN_DBITTIME_ADDR, 1000);
      FLASH_ByteWrite (CAN_GPIO1_MODE_ADDR, 0);
      FLASH_ByteWrite (CAN_GPIO2_MODE_ADDR, 0);
      FLASH_ByteWrite (CAN_OPMODE_ADDR, 0);
      FLASH_ByteWrite (START_ADDR, 0);
    }
}

void
CANFDSPI_TransmitChannelConfigure ()
{
  uint8_t channel = FLASH_ByteRead (CAN_TXCH_ADDR);
  uint8_t payload = FLASH_ByteRead (CAN_PAYLOAD_ADDR);
  CAN_TX_FIFO_CONFIG config;
  config.PayLoadSize = convert_payload (payload);
  DRV_CANFDSPI_TransmitChannelConfigure (0, channel, &config);
}
void
CANFDSPI_ReceiveChannelConfigure ()
{

  uint8_t channel = FLASH_ByteRead (CAN_RXCH_ADDR);
  uint8_t payload = FLASH_ByteRead (CAN_PAYLOAD_ADDR);
  CAN_RX_FIFO_CONFIG config;
  config.PayLoadSize = convert_payload (payload);
  DRV_CANFDSPI_ReceiveChannelConfigure (0, channel, &config);
}

void
CANFDSPI_FilterObjectConfigure ()
{
  uint8_t filter = FLASH_ByteRead (CAN_FILTER_ADDR);
  uint8_t filterid = FLASH_ByteRead (CAN_FILTER_ID_ADDR);
  CAN_FILTEROBJ_ID id =
    { 0 };
  id.bF1.SID = filterid;
  DRV_CANFDSPI_FilterObjectConfigure (0, filter, &id);
}

void
CANFDSPI_FilterMaskConfigure ()
{
  uint8_t filter = FLASH_ByteRead (CAN_FILTER_ADDR);
  uint8_t maskid = FLASH_ByteRead (CAN_MASK_ID_ADDR);
  CAN_MASKOBJ_ID id =
    { 0 };
  id.bF1.MSID = maskid;
  id.bF2.MIDE = 0;
  DRV_CANFDSPI_FilterMaskConfigure (0, filter, &id);
}

void
CANFDSPI_FilterToFifoLink ()
{
  uint8_t filter = FLASH_ByteRead (CAN_FILTER_ADDR);
  uint8_t channel = FLASH_ByteRead (CAN_RXCH_ADDR);
  DRV_CANFDSPI_FilterToFifoLink (0, filter, channel, true);
}

void
CANFDSPI_ReceiveChannelEventEnable ()
{
  uint8_t channel = FLASH_ByteRead (CAN_RXCH_ADDR);
  DRV_CANFDSPI_ReceiveChannelEventEnable (0, channel,
                                          CAN_RX_FIFO_NOT_EMPTY_EVENT);
}
void
CANFDSPI_NominalBitTimeConfigure ()
{
  uint16_t nlbitTime = Flash_2BRead (CAN_NBITTIME_ADDR);
  uint16_t dbbitTime = Flash_2BRead (CAN_DBITTIME_ADDR);
  uint8_t bitTime = convert_bittime (nlbitTime, dbbitTime);
  DRV_CANFDSPI_NominalBitTimeConfigure (0, bitTime, CAN_SYSCLK_20M);
  DRV_CANFDSPI_DataBitTimeConfigure (0, bitTime, CAN_SYSCLK_20M);
}

void
CANFDSPI_GpioModeConfigure ()
{
  uint8_t gpio1 = FLASH_ByteRead (CAN_GPIO1_MODE_ADDR);
  uint8_t gpio2 = FLASH_ByteRead (CAN_GPIO2_MODE_ADDR);
  DRV_CANFDSPI_GpioModeConfigure (0, gpio1, gpio2);
}

void
CANFDSPI_OperationModeSelect ()
{
  uint8_t opmode = FLASH_ByteRead (CAN_OPMODE_ADDR);
  DRV_CANFDSPI_OperationModeSelect (0, opmode);
}

void
CANFDSPI_ReceiveChannelEventGet (CAN_RX_FIFO_EVENT *rxFlags)
{
  uint8_t channel = FLASH_ByteRead (CAN_RXCH_ADDR);
  DRV_CANFDSPI_ReceiveChannelEventGet (0, channel, rxFlags);
}

void
CANFDSPI_ReceiveMessageGet (CAN_RX_MSGOBJ *rxObj, uint8_t *rxd, uint8_t nBytes)
{
  uint8_t channel = FLASH_ByteRead (CAN_RXCH_ADDR);
  DRV_CANFDSPI_ReceiveMessageGet (0, channel, rxObj, rxd, nBytes);
}

void
Update_CANNDTR (uint16_t configdata)
{
  if ((configdata == 125) | (configdata == 250) | (configdata == 500)
      | (configdata == 1000))
    {
      FLASH_Clear (CAN_NBITTIME_ADDR, 2);
      Flash_2BWrite (CAN_NBITTIME_ADDR, configdata);
      Uart_OK ();
    }
  else
    {
      Print_Invalid_data ();
    }
}

void
Update_CANDDTR (uint16_t configdata)
{
  uint16_t nlbitTime = Flash_2BRead (CAN_NBITTIME_ADDR);
  switch (nlbitTime)
    {
    case 125:
      if (configdata == 500)
        {
          FLASH_Clear (CAN_DBITTIME_ADDR, 2);
          Flash_2BWrite (CAN_DBITTIME_ADDR, configdata);
          Uart_OK ();
        }
      else
        {
          Print_Invalid_data ();
        }
      break;
    case 500:
      if ((configdata == 1000) | (configdata == 2000) | (configdata == 3000)
          | (configdata == 4000) | (configdata == 5000) | (configdata == 6700)
          | (configdata == 8000) | (configdata == 1000))
        {
          FLASH_Clear (CAN_DBITTIME_ADDR, 2);
          Flash_2BWrite (CAN_DBITTIME_ADDR, configdata);
          Uart_OK ();
        }
      else
        {
          Print_Invalid_data ();
        }
      break;
    case 250:
      if ((configdata == 500) | (configdata == 833) | (configdata == 1000)
          | (configdata == 1500) | (configdata == 2000) | (configdata == 3000)
          | (configdata == 4000))
        {
          FLASH_Clear (CAN_DBITTIME_ADDR, 2);
          Flash_2BWrite (CAN_DBITTIME_ADDR, configdata);
          Uart_OK ();
        }
      else
        {
          Print_Invalid_data ();
        }
      break;
    case 1000:
      if ((configdata == 4000) | (configdata == 8000))
        {
          FLASH_Clear (CAN_DBITTIME_ADDR, 2);
          Flash_2BWrite (CAN_DBITTIME_ADDR, configdata);
          Uart_OK ();
        }
      else
        {
          Print_Invalid_data ();
        }
      break;

    }

}

void
Update_CANGPIO1MODE (uint16_t configdata)
{
  if ((configdata == 0) | (configdata == 1))
    {
      FLASH_Clear (CAN_GPIO1_MODE_ADDR, 1);
      FLASH_ByteWrite (CAN_GPIO1_MODE_ADDR, configdata);
      Uart_OK ();
    }
  else
    {
      Print_Invalid_data ();
    }
}

void
Update_CANGPIO2MODE (uint16_t configdata)
{
  if ((configdata == 0) | (configdata == 1))
    {
      FLASH_Clear (CAN_GPIO2_MODE_ADDR, 1);
      FLASH_ByteWrite (CAN_GPIO2_MODE_ADDR, configdata);
      Uart_OK ();
    }
  else
    {
      Print_Invalid_data ();
    }
}

void
Update_CANOPMODE (uint16_t configdata)
{
  if ((configdata == 0) | (configdata == 1) | (configdata == 2)
      | (configdata == 3) | (configdata == 4) | (configdata == 5)
      | (configdata == 6) | (configdata == 7))
    {
      FLASH_Clear (CAN_OPMODE_ADDR, 1);
      FLASH_ByteWrite (CAN_OPMODE_ADDR, configdata);
      Uart_OK ();
    }
  else
    {
      Print_Invalid_data ();
    }
}

void
Update_CANPAYLOADSIZE (uint16_t configdata)
{
  if ((configdata == 8) | (configdata == 12) | (configdata == 16)
      | (configdata == 20) | (configdata == 24) | (configdata == 32)
      | (configdata == 48) | (configdata == 64))
    {
      FLASH_Clear (CAN_PAYLOAD_ADDR, 1);
      FLASH_ByteWrite (CAN_PAYLOAD_ADDR, configdata);
      Uart_OK ();
    }
  else
    {
      Print_Invalid_data ();
    }
}

uint8_t
convert_bittime (uint16_t nlbitTime, uint16_t dbbitTime)
{
  uint8_t bittime;
  switch (nlbitTime)
    {
    case 500:
      if (dbbitTime == 1000)
        {
          bittime = 0;
        }
      else if (dbbitTime == 2000)
        {
          bittime = 1;
        }
      else if (dbbitTime == 3000)
        {
          bittime = 2;
        }
      else if (dbbitTime == 4000)
        {
          bittime = 3;
        }
      else if (dbbitTime == 5000)
        {
          bittime = 4;
        }
      else if (dbbitTime == 6700)
        {
          bittime = 5;
        }
      else if (dbbitTime == 8000)
        {
          bittime = 6;
        }
      else if (dbbitTime == 10000)
        {
          bittime = 7;
        }
      break;
    case 1000:
      if (dbbitTime == 4000)
        {
          bittime = 15;
        }
      else if (dbbitTime == 8000)
        {
          bittime = 16;
        }
      break;
    case 125:
      if (dbbitTime == 500)
        {
          bittime = 17;
        }
      break;
    }
  return bittime;
}
uint8_t
convert_payload (uint8_t payload)
{
  uint8_t load;
  switch (payload)
    {
    case 32:
      load = 5;
      break;
    case 48:
      load = 6;
      break;
    case 64:
      load = 7;
      break;
    default:
      load = (payload / 4) - 2;
      break;
    }
  return load;
}

uint8_t
convert_dlc (uint8_t size)
{
  uint8_t load;

  if (size < 9)
    {
      load = size;
    }
  else if (size == 48)
    {
      load = 6;
    }
  else if (size == 64)
    {
      load = 7;
    }
  else
    {
      load = (size / 4) + 6;
    }
  return load;
}

