//-----------------------------------------------------------------------------
// SPI0_Lib_Master.c
//-----------------------------------------------------------------------------
// Copyright 2014 Silicon Laboratories, Inc.
// http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt
//
// Program Description:
//
// This example demonstrates the SPI interface in 4-wire, master mode using
// the SPI peripheral driver library. This example is intended to be used
// with the SPI_Lib_Slave example.
//
// The SPI clock in this example is limited to 1 MHz when used with the
// SPI0_Slave code example. During a SPI_Read, the slave needs some time to
// interpret the command and write the appropriate data to the SPI0DAT
// register, and the slave no longer has enough time to complete the
// SPI_READ_BUFFER command with a clock greater than 1 MHz. For faster SPI
// clocks, a dummy byte between the command and the first byte of Read data
// will be required.
//
// Resources:
//   SYSCLK - 24.5 MHz HFOSC / 1
//   SPI    - Master
//   P0.6   - SPI SCK
//   P0.7   - SPI MISO
//   P1.0   - SPI MOSI
//   P1.1   - SPI NSS
//   P1.4   - LED
//
//-----------------------------------------------------------------------------
// How To Test: EFM8BB1 LCK (SPI Master) + EFM8BB1 LCK (SPI Slave)
//-----------------------------------------------------------------------------
// 1) Connect the first EFM8BB1 LCK SPI pins to the second EFM8BB1 LCK running
//    the corresponding SPI_Slave code.
// 2) Connect the EFM8BB1 LCK board to a PC using a micro USB cable.
// 3) Compile and download code to the first EFM8BB1 LCK board.
//    In Simplicity Studio IDE, select Run -> Debug from the menu bar,
//    click the Debug button in the quick menu, or press F11.
// 4) Run the code.
//    In Simplicity Studio IDE, select Run -> Resume from the menu bar,
//    click the Resume button in the quick menu, or press F8.
// 5) If the communication passes, the LEDs on both the Master
//    and Slave boards will blink slowly. If it fails, the LEDs will be off.
//
// Target:         EFM8BB1
// Tool chain:     Generic
//
// Release 0.1 (ST)
//    - Initial Revision
//    - 10 OCT 2014
//

//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include <EFM8BB2_FlashUtils.h>
#include "stdint.h"
#include "InitDevice.h"
#include <SI_EFM8BB2_Register_Enums.h>
#include "can_api.h"
#include "can_defines.h"
//#include "uart_0.h"
#include "uart.h"
//#include "Flash_canconfig.h"
#include <string.h>
#include <stdio.h>
//#include <inttypes.h>
//#include "retargetserial.h"

SI_SBIT(LED, SFR_P1, 2);

uint16_t ext_msg_id_low = 0, ext_msg_id_high = 0, msg_id = 0;
uint8_t is_ext_id = 0, rxd[64];
//uint16_t msg_id = 0;
//uint8_t rxd[64];

//char* CANNDTR= "CANNDTR";
//-----------------------------------------------------------------------------
// Function Prototypes
//-----------------------------------------------------------------------------

void
Delay (void);
void
Device_Init ();
void
Application ();
void
CANFD_Config_Mode ();
void
CANFD_Data_Mode ();
//void
//CAN_Message_Send ();
void
Display_CAN_Message (uint8_t nBytes);
void
Read_CAN_Message (uint16_t *msg_id, uint8_t *rxd, uint8_t nBytes);

//CAN_RX_MSGOBJ rxObj;
//uint8_t rxd[MAX_DATA_BYTES];
//SI_SBIT(LED, SFR_P1, 4);

//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

// Typical Write:
//
//              | 1st sent | 2nd sent | 3rd sent |   ...    | last sent |
//              ---------------------------------------------------------
//  Master NSSv | Command  |   Data1  |   Data2  |   ...    |   DataN   |  NSS^
//  Slave       |   N/A    |    N/A   |    N/A   |   ...    |    N/A    |
//
// Typical Read:
//
//              | 1st sent | 2nd sent | 3rd sent |   ...    | last sent |
//              ---------------------------------------------------------
//  Master NSSv | Command  |   dummy  |   dummy  |   ...    |   dummy   |  NSS^
//  Slave       |   N/A    |   Data1  |   Data2  |   ...    |   DataN   |
//

//-----------------------------------------------------------------------------
// SiLabs_Startup() Routine
// ----------------------------------------------------------------------------
// This function is called immediately after reset, before the initialization
// code is run in SILABS_STARTUP.A51 (which runs before main() ). This is a
// useful place to disable the watchdog timer, which is enable by default
// and may trigger before main() in some instances.
//-----------------------------------------------------------------------------
void
SiLabs_Startup (void)
{
  // Disable the watchdog here
}

//verify spi comm, if fails blink led pin
void
mcp2518_spi_verify ()
{
  uint32_t testword = 0;
  DRV_CANFDSPI_WriteWord (0, 0x404, 0x55bb66cc);
  DRV_CANFDSPI_ReadWord (0, 0x404, &testword);
  if (testword != 0x55bb66cc)
    {
      while (1)
        {
          LED = 1;
          Delay ();
          LED = 0;
          Delay ();
        }
    }
}

void
mcp2518_initialize ()
{
  CANFD_Flashdata ();
  DRV_CANFDSPI_Reset (0);                             //Resetting CAN Controller
  DRV_CANFDSPI_EccEnable (0);                         //Enabling error detection
  DRV_CANFDSPI_RamInit (0, 0xff);                          // RAM Initialization
  //CAN Transmit
  DRV_CANFDSPI_Configure (0);
  CANFDSPI_TransmitChannelConfigure ();    // Transmitting CAN data on channel 2
  //CAN Receive
  CANFDSPI_ReceiveChannelConfigure ();        // receiving CAN data on channel 1
  CANFDSPI_FilterObjectConfigure ();
  CANFDSPI_FilterMaskConfigure ();
  CANFDSPI_FilterToFifoLink ();                      // filtering the RX channel
  CANFDSPI_NominalBitTimeConfigure ();
  //CANFDSPI_GpioModeConfigure ();
  CANFDSPI_ReceiveChannelEventEnable ();
  DRV_CANFDSPI_ModuleEventEnable (0, 3);
  CANFDSPI_OperationModeSelect ();

}

void
CAN_Message_Send (char *str)
{

  CAN_TX_FIFO_EVENT txFlags;
  CAN_TX_MSGOBJ txObj =
    { 0 };
  uint8_t txd[64];
  uint32_t msg;
  uint8_t size;

  parse_message (str, &msg, txd, &size);

  txObj.word[0] = 0;
  txObj.word[1] = 0;
  txObj.word[2] = 0;
  //txObj.word[0] = 0x51010000;
  txObj.byte[0] = *(((uint8_t*) &msg) + 3);
  txObj.byte[1] = *(((uint8_t*) &msg) + 2);
  txObj.byte[2] = *(((uint8_t*) &msg) + 1);
  txObj.byte[3] = *(((uint8_t*) &msg) + 0);
  //txObj.word[1] = 0x08000000;
  txObj.byte[4] = convert_dlc (size);

  do
    {
      DRV_CANFDSPI_TransmitChannelEventGet (0, CAN_FIFO_CH2, &txFlags);
    }
  while (!(txFlags & CAN_TX_FIFO_NOT_FULL_EVENT));

  DRV_CANFDSPI_TransmitChannelLoad (0, CAN_FIFO_CH2, &txObj, txd, size, true);

}

void
CANFD_Display_MSG ()
{
  uint8_t numbytes = FLASH_ByteRead (CAN_PAYLOAD_ADDR);
  while (1)
    {
      Read_CAN_Message (&msg_id, rxd, numbytes);
      Delay ();
      Display_CAN_Message (numbytes);
      UART_print ("\n", 1);
    }
}

//-----------------------------------------------------------------------------
// Main Routine
//-----------------------------------------------------------------------------
void
main (void)
{
  Device_Init ();

  Application ();

}

void
Device_Init ()
{
  enter_DefaultMode_from_RESET ();

  IE_EA = 1;
  //UART0_init (UART0_RX_ENABLE, UART0_WIDTH_8, UART0_MULTIPROC_DISABLE);

  mcp2518_spi_verify ();
  //mcp2518_initialize ();
}

void
Application ()
{
  char str[4] = "\0";
  while (1)
    {
      UART_Read (str);
      if (strcmp (str, "+++") == 0)
        {
          CANFD_Config_Mode ();
        }
      else if (strcmp (str, "---") == 0)
        {
          mcp2518_initialize ();
          Delay ();
          CANFD_Data_Mode ();

        }
      else
        {
          Print_Invalid ();
        }
    }
}

void
CANFD_Config_Mode ()
{
  char command[30] = "\0";
  uint16_t canconfig_data;
  //uint8_t flag = 0;
  char *str;
  while (1)
    {
      UART_Read (command);
      Capitalize_Command (command);
      str = parse_command (command, &canconfig_data);
      if (strcmp (str, "CANNDTR") == 0)
        {
          Update_CANNDTR (canconfig_data);
        }
      else if (strcmp (str, "CANDDTR") == 0)
        {
          Update_CANDDTR (canconfig_data);
        }
      else if (strcmp (str, "CANPAYLOADSIZE") == 0)
        {
          Update_CANPAYLOADSIZE (canconfig_data);
        }
      else if (strcmp (str, "CANOPMODE") == 0)
        {
          Update_CANOPMODE (canconfig_data);
        }
      else if (strcmp (str, "RESET") == 0)
        {
          FLASH_Clear (START_ADDR, 1);
          Uart_OK ();
        }
      else if (strcmp (command, "EXIT") == 0)
        {
          Uart_OK ();
          break;
        }
      else
        {
          Print_Invalid ();
        }
    }
}

void
CANFD_Data_Mode ()
{
  char str[200];
  while (1)
    {
      UART_Read (str);
      CAN_Message_Send (str);
      if (strcmp (str, "+-") == 0)
        {
          CANFD_Display_MSG ();
        }
      else if (strcmp (str, "EXIT") == 0)
        {
          break;
        }
      else
        {
          Print_Invalid ();
        }
    }
}

void
Read_CAN_Message (uint16_t *msg_id, uint8_t *rxd, uint8_t nBytes)
{
  CAN_RX_FIFO_EVENT rxFlags;
  CAN_RX_MSGOBJ rxObj;

  CANFDSPI_ReceiveChannelEventGet (&rxFlags); //reading RX channel 1 status through SPI

  if (rxFlags & CAN_RX_FIFO_NOT_EMPTY_EVENT) // if the RX channel is not empty
    {

      CANFDSPI_ReceiveMessageGet (&rxObj, rxd, nBytes); //rxObj ->  Msg id and rxd-> message data
    }
  *msg_id = rxObj.byte[0] + (((uint16_t) (rxObj.byte[1] & 0x07)) << 8); // extracting CAN Message id
  ext_msg_id_low = rxObj.byte[0] + (((uint16_t) (rxObj.byte[1])) << 8);
  ext_msg_id_high = rxObj.byte[2] + (((uint16_t) (rxObj.byte[3] & 0x1F)) << 8);
  is_ext_id = (rxObj.byte[4] >> 4) & 0x01;
}

void
Display_CAN_Message (uint8_t nBytes)
{
  int i;
  char hex_data[3];
  char id[6];
  char id_high[8];
  if (is_ext_id == 0)
    {
      sprintf (id, "0x%03X", msg_id);
      //id[5] = '\0';
      UART_print (id, 5);
      UART_print ("            ", 12);
    }
  else
    {
      sprintf (id_high, "0x%04X", ext_msg_id_high);
      //id_high[6] = '\0';
      UART_print (id_high, 6);
      sprintf (id, "%04X", ext_msg_id_low);
      //id[4] = '\0';
      UART_print (id, 4);
      UART_print ("       ", 7);
    }
  //UART_print ("0x", 2);
  //UART_print (rxd, nBytes);
  for (i = nBytes - 1; i >= 0; i--)
    {
      if (rxd[i] == 0)
        {
          UART_print ("00", 2);
        }
      else
        {
          sprintf (hex_data, "%02X", rxd[i]);
          hex_data[2] = '\0';
          UART_print (hex_data, 3);
        }
      UART_print (" ", 1);
    }
  UART_print ("\n", 1);
}

//-----------------------------------------------------------------------------
// Support Routines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Delay
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Delay for little while (used for blinking the LEDs)
//
//-----------------------------------------------------------------------------
void
Delay (void)
{
  uint8_t count;
  uint16_t c;
  for (count = 15; count > 0; count--)
    for (c = 1000; c > 0; c--)
      ;
}

void
SPI0_transferCompleteCb (void)
{

}

void
UART0_receiveCompleteCb ()
{

}

void
UART0_transmitCompleteCb ()
{
}
