/*
 * uart.c
 *
 *  Created on: Dec 20, 2023
 *      Author: vgnsi
 */

#include <SI_EFM8BB2_Register_Enums.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
//#include <uart.h>

//char *str_invalid = "INVALID";
//#define Invalid 14


void
UART_print (char *str, int size)
{
  int i;
  for (i = 0; i < size; i++)
    {
      SBUF0 = str[i];
      while (SCON0_TI == 0)
        ;   //wait for transmission completion
      SCON0_TI = 0;

    }
}

void
UART_Read (char *s)
{
  int i = 0;
  while (s[i - 1] != '\n')
    {
      if (SCON0_RI == 1)
        {
          s[i] = SBUF0;
          SCON0_RI = 0;
          i++;
        }
    }
  s[i - 1] = '\0';
}

void
Print_Invalid ()
{
  UART_print ("INVALID COMMAD", 14);
  UART_print ("\n", 1);
}

void
Print_Invalid_data ()
{
  UART_print ("INVALID DATA", 12);
  UART_print ("\n", 1);
}

void
Uart_OK ()
{
  UART_print ("OK", 2);
  UART_print ("\n", 1);
}

void
uart_newline ()
{
  UART_print ("\n", 1);
}
void
Capitalize_Command (char *string)
{
  int j = 0;
  while (string[j])
    {
      string[j] = toupper (string[j]);
      j++;
    }
}

char*
parse_command (char *string, uint16_t *val)
{

  char *str = "AT+";
  char *token1;
  char *token;

  token = strtok (string, "=");

  if (strstr (token, str) == NULL)
    {
      return "INVALID";
    }
  token1 = strtok (NULL, "\0");
  /* parse the same string again */
  token = strtok (token, "+");
  token = strtok (NULL, "\0");

  *val = atoi (token1);
  return token;
}

void
parse_message (char *string, uint32_t *val, uint8_t *bytes, uint8_t *size)
{
  char *token1;
  char *token;
  uint8_t len, l;
  int i, j = 0;

  token = strtok (string, " ");
  *val = strtol (token, NULL, 16);
  token1 = strtok (NULL, "\0");
  token1 = strtok (token1, "x");
  token1 = strtok (NULL, "\0");
  len = strlen (token1);
  if (len % 2 != 0)
    {
      l = len + 1;
    }
  else
    {
      l = len;
    }
  *size = l * 0.5;

  // converting hex string of data into byte array
  for (i = (l / 2) - 1, j = 0; i >= 0; i--)
    {
      if ((j == 0) && (len % 2 != 0))
        {
          bytes[i] = 0 + (token1[j + 1] % 32 + 9) % 25;
          j = 1;
        }
      else
        {
          bytes[i] = (token1[j] % 32 + 9) % 25 * 16
              + (token1[j + 1] % 32 + 9) % 25;
          j += 2;
        }
    }
}
