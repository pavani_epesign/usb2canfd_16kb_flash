/*
 * defines.h
 *
 *  Created on: Jan 16, 2024
 *      Author: vgnsi
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include <stdint.h>

// Maximum Size of TX/RX Object
//#define MAX_MSG_SIZE 76
#define MAX_MSG_SIZE 32
#define CAN_TXQUEUE_CH0 CAN_FIFO_CH0
#define SPI_DEFAULT_BUFFER_LENGTH 32
//#define MAX_DATA_BYTES 16
// Maximum number of data bytes in message
//#define MAX_DATA_BYTES 16
//#define Nop() asm("nop")

#define START_ADDR              0x3f00
#define CAN_TXCH_ADDR           0x3f01
#define CAN_PAYLOAD_ADDR        0x3f02
#define CAN_FILTER_ADDR         0x3f03
#define CAN_FILTER_ID_ADDR      0x3f04
#define CAN_MASK_ID_ADDR        0x3f05
#define CAN_RXCH_ADDR           0x3f06
#define CAN_NBITTIME_ADDR       0x3f07
#define CAN_DBITTIME_ADDR       0x3f09
#define CAN_GPIO1_MODE_ADDR     0x3f0B
#define CAN_GPIO2_MODE_ADDR     0x3f0C
#define CAN_GPIO1_DD_ADDR       0x3f0D
#define CAN_GPIO2_DD_ADDR       0x3f0E
#define CAN_OPMODE_ADDR         0x3f0F

typedef enum
{
  CAN_FIFO_CH0,   // CAN_TXQUEUE_CH0
  CAN_FIFO_CH1,
  CAN_FIFO_CH2,
  CAN_FIFO_CH3,
  CAN_FIFO_CH4,
  CAN_FIFO_CH5,
  CAN_FIFO_CH6,
  CAN_FIFO_CH7,
  CAN_FIFO_CH8,
  CAN_FIFO_CH9,
  CAN_FIFO_CH10,
  CAN_FIFO_CH11,
  CAN_FIFO_CH12,
  CAN_FIFO_CH13,
  CAN_FIFO_CH14,
  CAN_FIFO_CH15,
  CAN_FIFO_CH16,
  CAN_FIFO_CH17,
  CAN_FIFO_CH18,
  CAN_FIFO_CH19,
  CAN_FIFO_CH20,
  CAN_FIFO_CH21,
  CAN_FIFO_CH22,
  CAN_FIFO_CH23,
  CAN_FIFO_CH24,
  CAN_FIFO_CH25,
  CAN_FIFO_CH26,
  CAN_FIFO_CH27,
  CAN_FIFO_CH28,
  CAN_FIFO_CH29,
  CAN_FIFO_CH30,
  CAN_FIFO_CH31,
  CAN_FIFO_TOTAL_CHANNELS
} CAN_FIFO_CHANNEL;

typedef enum
{
  CAN_NORMAL_MODE = 0x00,
  CAN_SLEEP_MODE = 0x01,
  CAN_INTERNAL_LOOPBACK_MODE = 0x02,
  CAN_LISTEN_ONLY_MODE = 0x03,
  CAN_CONFIGURATION_MODE = 0x04,
  CAN_EXTERNAL_LOOPBACK_MODE = 0x05,
  CAN_CLASSIC_MODE = 0x06,
  CAN_RESTRICTED_MODE = 0x07,
  CAN_INVALID_MODE = 0xFF
} CAN_OPERATION_MODE;

typedef union _CAN_RX_MSGOBJ
{

//  struct {
//        CAN_MSGOBJ_ID id;
//       CAN_RX_MSGOBJ_CTRL ctrl;
//        CAN_MSG_TIMESTAMP timeStamp;
//   } bF;
  uint32_t word[3];
  uint8_t byte[12];
} CAN_RX_MSGOBJ;

typedef enum
{
  CAN_RX_FIFO_NO_EVENT = 0,
  CAN_RX_FIFO_ALL_EVENTS = 0x0F,
  CAN_RX_FIFO_NOT_EMPTY_EVENT = 0x01,
  CAN_RX_FIFO_HALF_FULL_EVENT = 0x02,
  CAN_RX_FIFO_FULL_EVENT = 0x04,
  CAN_RX_FIFO_OVERFLOW_EVENT = 0x08
} CAN_RX_FIFO_EVENT;

//! CAN TX FIFO Event (Interrupts)

typedef enum
{
  CAN_TX_FIFO_NO_EVENT = 0,
  CAN_TX_FIFO_ALL_EVENTS = 0x17,
  CAN_TX_FIFO_NOT_FULL_EVENT = 0x01,
  CAN_TX_FIFO_HALF_FULL_EVENT = 0x02,
  CAN_TX_FIFO_EMPTY_EVENT = 0x04,
  CAN_TX_FIFO_ATTEMPTS_EXHAUSTED_EVENT = 0x10
} CAN_TX_FIFO_EVENT;

typedef enum
{
  CAN_FILTER0,
  CAN_FILTER1,
  CAN_FILTER2,
  CAN_FILTER3,
  CAN_FILTER4,
  CAN_FILTER5,
  CAN_FILTER6,
  CAN_FILTER7,
  CAN_FILTER8,
  CAN_FILTER9,
  CAN_FILTER10,
  CAN_FILTER11,
  CAN_FILTER12,
  CAN_FILTER13,
  CAN_FILTER14,
  CAN_FILTER15,
  CAN_FILTER16,
  CAN_FILTER17,
  CAN_FILTER18,
  CAN_FILTER19,
  CAN_FILTER20,
  CAN_FILTER21,
  CAN_FILTER22,
  CAN_FILTER23,
  CAN_FILTER24,
  CAN_FILTER25,
  CAN_FILTER26,
  CAN_FILTER27,
  CAN_FILTER28,
  CAN_FILTER29,
  CAN_FILTER30,
  CAN_FILTER31,
  CAN_FILTER_TOTAL,
} CAN_FILTER;

typedef struct _CAN_FILTEROBJ_ID
{
  struct
  {
    uint16_t SID :11;
    uint16_t EID1 :5;
  } bF1;
  struct
  {
    uint16_t EID2 :13;
    uint16_t SID11 :1;
    uint16_t EXIDE :1;
  } bF2;
} CAN_FILTEROBJ_ID;

typedef struct _CAN_MASKOBJ_ID
{
  struct
  {
    uint16_t MSID :11;
    uint16_t MEID1 :5;
  } bF1;
  struct
  {
    uint16_t MEID2 :13;
    uint16_t MSID11 :1;
    uint16_t MIDE :1;
  } bF2;
} CAN_MASKOBJ_ID;

typedef enum
{
  CAN_NO_EVENT = 0,
  CAN_ALL_EVENTS = 0xFF1F,
  CAN_TX_EVENT = 0x0001,
  CAN_RX_EVENT = 0x0002,
  CAN_TIME_BASE_COUNTER_EVENT = 0x0004,
  CAN_OPERATION_MODE_CHANGE_EVENT = 0x0008,
  CAN_TEF_EVENT = 0x0010,

  CAN_RAM_ECC_EVENT = 0x0100,
  CAN_SPI_CRC_EVENT = 0x0200,
  CAN_TX_ATTEMPTS_EVENT = 0x0400,
  CAN_RX_OVERFLOW_EVENT = 0x0800,
  CAN_SYSTEM_ERROR_EVENT = 0x1000,
  CAN_BUS_ERROR_EVENT = 0x2000,
  CAN_BUS_WAKEUP_EVENT = 0x4000,
  CAN_RX_INVALID_MESSAGE_EVENT = 0x8000
} CAN_MODULE_EVENT;

//! Transmit Bandwidth Sharing

typedef enum
{
  CAN_TXBWS_NO_DELAY,
  CAN_TXBWS_2,
  CAN_TXBWS_4,
  CAN_TXBWS_8,
  CAN_TXBWS_16,
  CAN_TXBWS_32,
  CAN_TXBWS_64,
  CAN_TXBWS_128,
  CAN_TXBWS_256,
  CAN_TXBWS_512,
  CAN_TXBWS_1024,
  CAN_TXBWS_2048,
  CAN_TXBWS_4096
} CAN_TX_BANDWITH_SHARING;

//! Wake-up Filter Time

typedef enum
{
  CAN_WFT00, CAN_WFT01, CAN_WFT10, CAN_WFT11
} CAN_WAKEUP_FILTER_TIME;

//! Data Byte Filter Number

typedef enum
{
  CAN_DNET_FILTER_DISABLE = 0,
  CAN_DNET_FILTER_SIZE_1_BIT,
  CAN_DNET_FILTER_SIZE_2_BIT,
  CAN_DNET_FILTER_SIZE_3_BIT,
  CAN_DNET_FILTER_SIZE_4_BIT,
  CAN_DNET_FILTER_SIZE_5_BIT,
  CAN_DNET_FILTER_SIZE_6_BIT,
  CAN_DNET_FILTER_SIZE_7_BIT,
  CAN_DNET_FILTER_SIZE_8_BIT,
  CAN_DNET_FILTER_SIZE_9_BIT,
  CAN_DNET_FILTER_SIZE_10_BIT,
  CAN_DNET_FILTER_SIZE_11_BIT,
  CAN_DNET_FILTER_SIZE_12_BIT,
  CAN_DNET_FILTER_SIZE_13_BIT,
  CAN_DNET_FILTER_SIZE_14_BIT,
  CAN_DNET_FILTER_SIZE_15_BIT,
  CAN_DNET_FILTER_SIZE_16_BIT,
  CAN_DNET_FILTER_SIZE_17_BIT,
  CAN_DNET_FILTER_SIZE_18_BIT
} CAN_DNET_FILTER_SIZE;

//! FIFO Payload Size

typedef enum
{
  CAN_PLSIZE_8,
  CAN_PLSIZE_12,
  CAN_PLSIZE_16,
  CAN_PLSIZE_20,
  CAN_PLSIZE_24,
  CAN_PLSIZE_32,
  CAN_PLSIZE_48,
  CAN_PLSIZE_64
} CAN_FIFO_PLSIZE;


//typedef struct _CAN_CONFIG
//{
//  struct
//  {
//    uint16_t DNetFilterCount :5;
//    uint16_t IsoCrcEnable :1;
//    uint16_t ProtocolExceptionEventDisable :1;
//    uint16_t WakeUpFilterEnable :1;
//    uint16_t WakeUpFilterTime :2;
//    uint16_t BitRateSwitchDisable :1;
//  } bF1;
//  struct
//  {
//    uint16_t RestrictReTxAttempts :1;
//    uint16_t EsiInGatewayMode :1;
//    uint16_t SystemErrorToListenOnly :1;
//    uint16_t StoreInTEF :1;
//    uint16_t TXQEnable :1;
//    uint16_t AbortAllTx :1;
//    uint16_t TxBandWidthSharing :4;
//  } bF2;
//} CAN_CONFIG;

//! CAN Transmit Channel Configure

typedef struct _CAN_TX_FIFO_CONFIG
{
//  uint16_t RTREnable :1;
//  uint16_t TxPriority :5;
//  uint16_t TxAttempts :2;
//  uint16_t FifoSize :5;
  uint8_t PayLoadSize :3;
  //uint16_t word;
} CAN_TX_FIFO_CONFIG;

//! CAN Transmit Queue Configure

typedef struct _CAN_RX_FIFO_CONFIG
{
//  uint16_t RxTimeStampEnable :1;
//  uint16_t FifoSize :5;
  uint8_t PayLoadSize :3;
} CAN_RX_FIFO_CONFIG;

//! CAN Transmit Event FIFO Configure


typedef struct _CAN_TX_MSGOBJ_CTRL
{
  struct
  {
    uint16_t DLC :4;
    uint16_t IDE :1;
    uint16_t RTR :1;
    uint16_t BRS :1;
    uint16_t FDF :1;
    uint16_t ESI :1;
    uint16_t SEQ1 :7;
  } bF1;
  struct
  {
    uint16_t SEQ2 :16;
  } bF2;

} CAN_TX_MSGOBJ_CTRL;

typedef struct _CAN_MSGOBJ_ID
{
  struct
  {
    uint16_t SID :11;
    uint16_t EID1 :5;

  }bF1;
  struct
  {
    uint16_t EID2 :13;
    uint16_t SID11 :1;
    uint16_t unimplemented1 :2;
  }bF2;
} CAN_MSGOBJ_ID;

//! CAN RX Message Object Control

typedef struct _CAN_RX_MSGOBJ_CTRL
{
  /* uint32_t DLC : 4;
   uint32_t IDE : 1;
   uint32_t RTR : 1;
   uint32_t BRS : 1;
   uint32_t FDF : 1;
   uint32_t ESI : 1;
   uint32_t unimplemented1 : 2;
   uint32_t FilterHit : 5;
   uint32_t unimplemented2 : 16;*/
  uint32_t word;
} CAN_RX_MSGOBJ_CTRL;

//! CAN Message Time Stamp

//! CAN TX Message Object

typedef union _CAN_TX_MSGOBJ
{
//  struct
//  {
//    CAN_MSGOBJ_ID id;
//    CAN_TX_MSGOBJ_CTRL ctrl;
//    CAN_MSG_TIMESTAMP timeStamp;
//  } bF;
  uint32_t word[3];
  uint8_t byte[12];
} CAN_TX_MSGOBJ;


typedef enum
{
  CAN_RX_FIFO_EMPTY = 0,
  CAN_RX_FIFO_STATUS_MASK = 0x0F,
  CAN_RX_FIFO_NOT_EMPTY = 0x01,
  CAN_RX_FIFO_HALF_FULL = 0x02,
  CAN_RX_FIFO_FULL = 0x04,
  CAN_RX_FIFO_OVERFLOW = 0x08
} CAN_RX_FIFO_STATUS;

//! CAN TX FIFO Status

typedef enum
{
  CAN_TX_FIFO_FULL = 0,
  CAN_TX_FIFO_STATUS_MASK = 0x1F7,
  CAN_TX_FIFO_NOT_FULL = 0x01,
  CAN_TX_FIFO_HALF_FULL = 0x02,
  CAN_TX_FIFO_EMPTY = 0x04,
  CAN_TX_FIFO_ATTEMPTS_EXHAUSTED = 0x10,
  CAN_TX_FIFO_ERROR = 0x20,
  CAN_TX_FIFO_ARBITRATION_LOST = 0x40,
  CAN_TX_FIFO_ABORTED = 0x80,
  CAN_TX_FIFO_TRANSMITTING = 0x100
} CAN_TX_FIFO_STATUS;

//! CAN TEF FIFO Status


typedef enum
{
  CAN_500K_1M, // 0x00
  CAN_500K_2M, // 0x01
  CAN_500K_3M,
  CAN_500K_4M,
  CAN_500K_5M, // 0x04
  CAN_500K_6M7,
  CAN_500K_8M, // 0x06
  CAN_500K_10M,
  CAN_250K_500K, // 0x08
  CAN_250K_833K,
  CAN_250K_1M,
  CAN_250K_1M5,
  CAN_250K_2M,
  CAN_250K_3M,
  CAN_250K_4M,
  CAN_1000K_4M, // 0x0f
  CAN_1000K_8M,
  CAN_125K_500K // 0x11
} CAN_BITTIME_SETUP;

//! CAN Nominal Bit Time Setup


typedef enum
{
  CAN_SSP_MODE_OFF, CAN_SSP_MODE_MANUAL, CAN_SSP_MODE_AUTO
} CAN_SSP_MODE;

//! CAN Error State

typedef enum
{
  CAN_ERROR_FREE_STATE = 0,
  CAN_ERROR_ALL = 0x3F,
  CAN_TX_RX_WARNING_STATE = 0x01,
  CAN_RX_WARNING_STATE = 0x02,
  CAN_TX_WARNING_STATE = 0x04,
  CAN_RX_BUS_PASSIVE_STATE = 0x08,
  CAN_TX_BUS_PASSIVE_STATE = 0x10,
  CAN_TX_BUS_OFF_STATE = 0x20
} CAN_ERROR_STATE;

//! CAN Time Stamp Mode Select

typedef enum
{
  CAN_TS_SOF = 0x00, CAN_TS_EOF = 0x01, CAN_TS_RES = 0x02
} CAN_TS_MODE;


//! GPIO Pin Position

typedef enum
{
  GPIO_PIN_0, GPIO_PIN_1
} GPIO_PIN_POS;

//! GPIO Pin Modes

typedef enum
{
  GPIO_MODE_INT, GPIO_MODE_GPIO
} GPIO_PIN_MODE;


//! GPIO Pin Directions
typedef enum
{
  GPIO_OUTPUT, GPIO_INPUT
} GPIO_PIN_DIRECTION;

typedef enum
{
  CAN_SYSCLK_40M, CAN_SYSCLK_20M, CAN_SYSCLK_10M
} CAN_SYSCLK_SPEED;
#endif /* DEFINES_H_ */
