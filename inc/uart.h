/*
 * uart.h
 *
 *  Created on: Dec 20, 2023
 *      Author: vgnsi
 */

#ifndef INC_UART_H_
#define INC_UART_H_

/* sending string of characters with size of string
 * Parameters: str  (string to send )
 *             size (size of string )*/
void
UART_print (char*str, int size);
void
UART_Read (char *str);
char*
parse_command (char *string, uint16_t *val);
void
parse_message (char *string, uint32_t *val, uint8_t *bytes, uint8_t *size);
void
Capitalize_Command (char *string);
void
Print_Invalid ();
void
Print_Invalid_data ();
void uart_newline();
void
Uart_OK ();
//void
//Get_data (char* str,int *pos);
#endif /* INC_UART_H_ */
