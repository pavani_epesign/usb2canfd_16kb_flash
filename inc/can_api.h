/*
 * api.h
 *
 *  Created on: Jan 16, 2024
 *      Author: vgnsi
 */

#ifndef API_H_
#define API_H_

// *****************************************************************************
// *****************************************************************************
// Section: Included Files

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "can_registers.h"
#include "can_defines.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility
extern "C" {
#endif
// DOM-IGNORE-END

typedef uint8_t CANFDSPI_MODULE_ID;

// *****************************************************************************
// *****************************************************************************
//! Reset DUT

int8_t
DRV_CANFDSPI_Reset (CANFDSPI_MODULE_ID index);

// *****************************************************************************
// *****************************************************************************
// Section: SPI Access Functions

// *****************************************************************************
//! SPI Read Byte

int8_t
DRV_CANFDSPI_ReadByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t *rxd);

// *****************************************************************************
//! SPI Write Byte

int8_t
DRV_CANFDSPI_WriteByte (CANFDSPI_MODULE_ID index, uint16_t address, uint8_t txd);

// *****************************************************************************
//! SPI Read Word

int8_t
DRV_CANFDSPI_ReadWord (CANFDSPI_MODULE_ID index, uint16_t address,
                       uint32_t *rxd);

// *****************************************************************************
//! SPI Write Word

int8_t
DRV_CANFDSPI_WriteWord (CANFDSPI_MODULE_ID index, uint16_t address,
                        uint32_t txd);

/// *****************************************************************************
//! SPI Read Word

int8_t
DRV_CANFDSPI_ReadHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                           uint16_t *rxd);

// *****************************************************************************
//! SPI Write Word

int8_t
DRV_CANFDSPI_WriteHalfWord (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint16_t txd);

// *****************************************************************************
//! SPI Read Byte Array

int8_t
DRV_CANFDSPI_ReadByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint8_t *rxd, uint16_t nBytes);

// *****************************************************************************
//! SPI Write Byte Array

int8_t
DRV_CANFDSPI_WriteByteArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint8_t *txd, uint16_t nBytes);

//! SPI Read Word Array

int8_t
DRV_CANFDSPI_ReadWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                            uint32_t *rxd, uint16_t nWords);

// *****************************************************************************
//! SPI Write Word Array

int8_t
DRV_CANFDSPI_WriteWordArray (CANFDSPI_MODULE_ID index, uint16_t address,
                             uint32_t *txd, uint16_t nWords);

int8_t
DRV_CANFDSPI_EccEnable (CANFDSPI_MODULE_ID index);
int8_t
DRV_CANFDSPI_Reset (CANFDSPI_MODULE_ID index);
int8_t
DRV_CANFDSPI_RamInit (CANFDSPI_MODULE_ID index, uint8_t d);
int8_t
DRV_CANFDSPI_Configure (CANFDSPI_MODULE_ID index);
int8_t
DRV_CANFDSPI_ReceiveChannelConfigure (CANFDSPI_MODULE_ID index,
                                      uint8_t channel,
                                      CAN_RX_FIFO_CONFIG *config);
int8_t
DRV_CANFDSPI_TransmitChannelConfigure (CANFDSPI_MODULE_ID index,
                                       uint8_t channel,
                                       CAN_TX_FIFO_CONFIG *config);
int8_t
DRV_CANFDSPI_ReceiveChannelUpdate (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel);
int8_t
DRV_CANFDSPI_ReceiveMessageGet (CANFDSPI_MODULE_ID index,
                                uint8_t channel, CAN_RX_MSGOBJ *rxObj,
                                uint8_t *rxd, uint8_t nBytes);
int8_t
DRV_CANFDSPI_ReceiveChannelEventGet (CANFDSPI_MODULE_ID index,
                                     uint8_t channel,
                                     CAN_RX_FIFO_EVENT *flags);
int8_t
DRV_CANFDSPI_NominalBitTimeConfigure (CANFDSPI_MODULE_ID index,
                               CAN_BITTIME_SETUP bitTime, uint8_t clk);
int8_t
DRV_CANFDSPI_DataBitTimeConfigure (CANFDSPI_MODULE_ID index,
                               CAN_BITTIME_SETUP bitTime, uint8_t clk);
int8_t
DRV_CANFDSPI_BitTimeConfigureNominal20MHz (CANFDSPI_MODULE_ID index,
                                           CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_BitTimeConfigureData20MHz (CANFDSPI_MODULE_ID index,
                                        CAN_BITTIME_SETUP bitTime);
int8_t
DRV_CANFDSPI_OperationModeSelect (CANFDSPI_MODULE_ID index,
                                  CAN_OPERATION_MODE opMode);
int8_t
DRV_CANFDSPI_ReceiveChannelEventGet (CANFDSPI_MODULE_ID index,
                                     CAN_FIFO_CHANNEL channel,
                                     CAN_RX_FIFO_EVENT *flags);
int8_t
DRV_CANFDSPI_FilterObjectConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                    CAN_FILTEROBJ_ID *id);
int8_t
DRV_CANFDSPI_FilterMaskConfigure (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                                  CAN_MASKOBJ_ID *mask);
int8_t
DRV_CANFDSPI_FilterToFifoLink (CANFDSPI_MODULE_ID index, CAN_FILTER filter,
                               CAN_FIFO_CHANNEL channel, bool enable);
int8_t
DRV_CANFDSPI_ReceiveChannelEventEnable (CANFDSPI_MODULE_ID index,
                                        CAN_FIFO_CHANNEL channel,
                                        CAN_RX_FIFO_EVENT flags);

int8_t
DRV_CANFDSPI_GpioModeConfigure (CANFDSPI_MODULE_ID index, GPIO_PIN_MODE gpio0,
                                GPIO_PIN_MODE gpio1);
int8_t
DRV_CANFDSPI_ModuleEventEnable (CANFDSPI_MODULE_ID index,
                                CAN_MODULE_EVENT flags);
int8_t
DRV_CANFDSPI_ReceiveChannelUpdate (CANFDSPI_MODULE_ID index,
                                   CAN_FIFO_CHANNEL channel);
int8_t DRV_CANFDSPI_TransmitChannelLoad(CANFDSPI_MODULE_ID index,
        CAN_FIFO_CHANNEL channel, CAN_TX_MSGOBJ* txObj,
        uint8_t *txd, uint32_t txdNumBytes, bool flush);
int8_t DRV_CANFDSPI_TransmitChannelUpdate(CANFDSPI_MODULE_ID index,
        CAN_FIFO_CHANNEL channel, bool flush);
int8_t DRV_CANFDSPI_TransmitChannelEventGet(CANFDSPI_MODULE_ID index,
        CAN_FIFO_CHANNEL channel, CAN_TX_FIFO_EVENT* flags);
void
CANFD_Flashdata ();
void
CANFDSPI_Configure ();
void
CANFDSPI_TransmitChannelConfigure ();
void
CANFDSPI_ReceiveChannelConfigure ();
void
CANFDSPI_FilterObjectConfigure ();
void
CANFDSPI_FilterMaskConfigure ();
void
CANFDSPI_FilterToFifoLink ();
void
CANFDSPI_NominalBitTimeConfigure ();
void
CANFDSPI_DataBitTimeConfigure ();
void
CANFDSPI_GpioModeConfigure ();
void
CANFDSPI_OperationModeSelect ();
void
CANFDSPI_ReceiveChannelEventGet (CAN_RX_FIFO_EVENT *rxFlags);
void
CANFDSPI_ReceiveChannelEventEnable ();
void
CANFDSPI_ReceiveMessageGet (CAN_RX_MSGOBJ *rxObj, uint8_t *rxd, uint8_t nBytes);
uint8_t
convert_bittime (uint16_t nlbitTime, uint16_t dbbitTime);
uint8_t
convert_payload (uint8_t payload);
uint8_t
convert_txbws (uint16_t txbws);
uint8_t
convert_dlc(uint8_t size);
void
Update_CANNDTR (uint16_t configdata);
void
Update_CANDDTR (uint16_t configdata);
void
Update_CANPAYLOADSIZE (uint16_t configdata);
void
Update_CANOPMODE (uint16_t configdata);
void
Update_CANGPIO2MODE (uint16_t configdata);
void
Update_CANGPIO1MODE (uint16_t configdata);

#endif /* API_H_ */
