################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
A51_UPPER_SRCS += \
../src/SILABS_STARTUP.A51 

C_SRCS += \
../src/EFM8BB2_FlashPrimitives.c \
../src/EFM8BB2_FlashUtils.c \
../src/Flash_canconfig.c \
../src/InitDevice.c \
../src/can_func.c \
../src/main.c \
../src/uart.c 

OBJS += \
./src/EFM8BB2_FlashPrimitives.OBJ \
./src/EFM8BB2_FlashUtils.OBJ \
./src/Flash_canconfig.OBJ \
./src/InitDevice.OBJ \
./src/SILABS_STARTUP.OBJ \
./src/can_func.OBJ \
./src/main.OBJ \
./src/uart.OBJ 


# Each subdirectory must supply rules for building sources it contributes
src/%.OBJ: ../src/%.c src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Keil 8051 Compiler'
	C51 "@$(patsubst %.OBJ,%.__i,$@)" || $(RC)
	@echo 'Finished building: $<'
	@echo ' '

src/EFM8BB2_FlashPrimitives.OBJ: C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashPrimitives.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h

src/EFM8BB2_FlashUtils.OBJ: C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashPrimitives.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashUtils.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h

src/Flash_canconfig.OBJ: C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_api.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_defines.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashPrimitives.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashUtils.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/uart.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDDEF.H C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDLIB.H C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_registers.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h

src/InitDevice.OBJ: C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/InitDevice.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h

src/%.OBJ: ../src/%.A51 src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Keil 8051 Assembler'
	AX51 "@$(patsubst %.OBJ,%.__ia,$@)" || $(RC)
	@echo 'Finished building: $<'
	@echo ' '

src/can_func.OBJ: C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_registers.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/peripheral_driver/inc/spi_0.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/efm8_config.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_api.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_defines.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDDEF.H C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDLIB.H C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h

src/main.OBJ: C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashUtils.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/InitDevice.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_api.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_defines.h C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/uart.h C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STRING.H C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDIO.H C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/EFM8BB2_FlashPrimitives.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDDEF.H C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDLIB.H C:/Users/vgnsi/SimplicityStudio/Projects/BB2_USB2CANFD/inc/can_registers.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h

src/uart.OBJ: C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Register_Enums.h C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STRING.H C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/STDLIB.H C:/SiliconLabs/SimplicityStudio/v5/developer/toolchains/keil_8051/9.60/INC/CTYPE.H C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/EFM8BB2/inc/SI_EFM8BB2_Defs.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/si_toolchain.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdint.h C:/SiliconLabs/SimplicityStudio/v5/developer/sdks/8051/v4.3.0/Device/shared/si8051Base/stdbool.h


